import "./todoList.css";
import "./todoItem.css";
import { ToDoItem } from "./todoItem";
import { useState } from "react";

export const ToDoList = () => {
  const initialThingsArray=[{id:1,description:'umyć psa'},{id:2,description:'zatankować samochód'},{id:3,description:'podbić świat'}]
  const [thingsArray, setThingsArray] = useState(initialThingsArray)
  const [inputValue, setInputValue] = useState("");

  let taskDescription = ""
  
  async function insertion(){
    if(taskDescription!=""){
      await waitFor(500)

      function makeId() {
        var result = '';
        let length = 10;
        var characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {result += characters.charAt(Math.floor(Math.random()*charactersLength));}
        return result;
      }
  
      setThingsArray(arr => [...arr, {id:makeId(),description: taskDescription}] ) 
      setInputValue("")
      document.getElementById('inputTask').value = ''
    }else{alert('opis zadania nie może być pusty')}
  }
  
  async function deletion(functionItem){
    await waitFor(500)
    setThingsArray( arr => arr.filter((arrayItem) => {return arrayItem.id !== functionItem.id}))
  }

  async function edition(thing,newDescription){
    if(newDescription!=""){
      await waitFor(500)
      setThingsArray( arr => arr.map((item) => {
        if(item.id !== thing.id){
          return item
        }else{
          return {id:thing.id,description:newDescription}
        }
      }))
    }else{alert('opis zadania nie może być pusty')}
  }
    
  function waitFor(time){
    return new Promise((resolve) => {setTimeout(resolve, time);})
  }
  
  return(
    <div className="itemsContainer">

      <div className="newTask">
        <label>
          <div>Nowe zadanko?</div>
          <input id="inputTask" placeholder="Wpisz coś!" onChange={event => {taskDescription=event.target.value}} />
        </label>

        <div className="custom-button" onClick={ async() => {insertion()} }> Dodaj! </div>
        
      </div>
      
      {thingsArray.map((item, index) => {return(<ToDoItem key={index} deletion={deletion} edition={edition} thingToDo={item} />)})}

    </div>
  )

}
