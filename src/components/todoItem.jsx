import { useState } from "react"
import "./todoItem.css"
import "./todoList.css"
import Modal from 'react-modal';

Modal.setAppElement('#root')

const customStyles = {
  content:{
    top:'50%',left:'50%',
    right:'auto',bottom:'auto',
    transform:'translate(-50%,-50%)',   
    backgroundColor:'rgba(255,225,107,0.1)'
  },
  overlay:{backgroundColor:'rgba(90,98,107,0.95)'},
};

export const ToDoItem = ({thingToDo, deletion, edition}) => {
  
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [isDone, setIsDone] = useState("")

  let newDescription = ""

  return(
    <div className="item">

      <div id={thingToDo.id} className={isDone}>Do zrobienia: {thingToDo.description}</div>
      <label>
        <input type="checkbox" onChange={(e) => {
          if(e.target.checked){
            setIsDone(val => val='itemDone')
          }else{
            setIsDone(val => val='')          
          }
        }} />
        Wykonane
      </label>
        
      <div className="buttons">
        <div className="custom-button" onClick={() => {deletion(thingToDo)}}>Usuń</div>
        <div className="custom-button" onClick={() => {setModalIsOpen(true)}}>Edytuj</div>
      </div>
    
      <Modal style={customStyles} isOpen={modalIsOpen} onRequestClose={() => {setModalIsOpen(false)}} >
        <div className="modalItem">

          <div>
            Nowy opis zadania:
          </div>

          <label>
            <input id="newTaskInput" placeholder="Wpisz coś!" onChange={event => {newDescription=event.target.value}} />
          </label>

          <div className="custom-button" onClick={() => {edition(thingToDo, newDescription);setModalIsOpen(false);}}>
            Edytuj
          </div>
          
        </div>
      </Modal>
      
    </div>
  )
}

